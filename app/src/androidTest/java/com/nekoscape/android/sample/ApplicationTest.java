package com.nekoscape.android.sample;

import android.app.Application;
import android.test.ApplicationTestCase;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@Config(emulateSdk = 18)
@RunWith(RobolectricTestRunner.class)
public class ApplicationTest {

    @Before
    public void setup() {
    }

    @After
    public void teardown() {
    }

    @Test
    public void testSample() {
        Assert.assertEquals("a", "a");
    }
}